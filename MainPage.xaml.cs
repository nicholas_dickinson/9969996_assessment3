﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using assessment3_9969996.Classes;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace assessment3_9969996
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            contacts.ItemsSource = loadAllTheData();
        }

        private void names_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (contacts.SelectedItem != null)
            {
                selectedName(contacts.SelectedItem.ToString());
            }

            
        }

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            menuPanel.IsPaneOpen = !menuPanel.IsPaneOpen;
        }


        private void newContact_Click(object sender, RoutedEventArgs e)
        {
            sqlFile.AddData(fnameBox.Text, lnameBox.Text, dobBox.Text, streetnoBox.Text, streetnameBox.Text, postcodeBox.Text, cityBox.Text, phone1Box.Text, phone2Box.Text, emailBox.Text);
            contacts.ItemsSource = loadAllTheData();
            clearAll();
        }

        private async void delete_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new Windows.UI.Popups.MessageDialog("Are you sure you want to delete this contact?");
            dialog.Title = "Deleting Contact";

            dialog.Commands.Add(new Windows.UI.Popups.UICommand("Yes") { Id = 0 });
            dialog.Commands.Add(new Windows.UI.Popups.UICommand("No") { Id = 1 });


            dialog.DefaultCommandIndex = 0;
            dialog.CancelCommandIndex = 1;


            var result = await dialog.ShowAsync();

            if ((int)result.Id == 0)
            {
                sqlFile.DeleteDate(userAccount.Text);
                contacts.ItemsSource = loadAllTheData();
                clearAll();

            }else { }


        }

        private void update_Click(object sender, RoutedEventArgs e)
        {
            

                sqlFile.UpdateData(fnameBox.Text, lnameBox.Text, dobBox.Text, streetnoBox.Text, streetnameBox.Text, postcodeBox.Text, cityBox.Text, phone1Box.Text, phone2Box.Text, emailBox.Text, userAccount.Text);
                contacts.ItemsSource = loadAllTheData();
                clearAll();

        }



        ////**/ DB  Methods /**////

        void selectedName(string name)
        {
            var command = $"Select * from tbl_people WHERE FNAME = '{name}' ";
            var b = sqlFile.ShowInList(command);

            userAccount.Text = b[0];

            fnameBox.Text = b[1];
            lnameBox.Text = b[2];
            dobBox.Text = b[3];
            streetnoBox.Text = b[4];
            streetnameBox.Text = b[5];
            postcodeBox.Text = b[6];
            cityBox.Text = b[7];
            phone1Box.Text = b[8];
            phone2Box.Text = b[9];
            emailBox.Text = b[10];
            
        }

        static List<string> loadAllTheData()
        {
            //Set the command and executes it and returns a list
            var command = $"Select FNAME from tbl_people";

            //Call the custom method
            var list = sqlFile.ShowInList(command);

            //Display the list (in this case a combo list)
            return list;
        }

        void clearAll()
        {
            fnameBox.Text = "";
            lnameBox.Text = "";
            dobBox.Text = "";
            streetnoBox.Text = "";
            streetnameBox.Text = "";
            postcodeBox.Text = "";
            cityBox.Text = "";
            phone1Box.Text = "";
            phone2Box.Text = "";
            emailBox.Text = "";
        }


    }
}
