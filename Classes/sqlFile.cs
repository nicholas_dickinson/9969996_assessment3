﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.Common;
using MySql.Data.Types;
using MySql.Data.MySqlClient;


namespace assessment3_9969996.Classes
{
    public class sqlFile
    {
       
        private static string host = "localhost";
        private static string user = "root";
        private static string pass = "root";
        private static string dbname = "gui_comp6001_16b_assn3";

        
        private static MySqlConnection Connection(string h, string u, string p, string n)
        {
            string serverConnection = String.Format($"Server={h};Database={n};Uid={u};Pwd={p};SslMode=None;charset=utf8");

            MySqlConnection connection = new MySqlConnection(serverConnection);

            return connection;
        }

        public static MySqlConnection conn()
        {
            var a = Connection(host, user, pass, dbname);
            return a;
        }

        public class Results
        {
            public string Row { get; set; }
        }

        public static List<Results> SelectCommand(string command, MySqlConnection connection)
        {
            
            EncodingProvider p;
            p = CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(p);

            List<Results> listTables = new List<Results>();

            using (connection)
            {
                connection.Open();

                MySqlCommand getCommand = connection.CreateCommand();
                getCommand.CommandText = command;
                using (MySqlDataReader reader = getCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        for (var i = 0; i < reader.FieldCount; i++)
                        {
                            listTables.Add(new Results() { Row = $"{reader.GetValue(i)}" });
                        }
                    }
                }
            }

            return listTables;
        }

        public static List<string> ShowInList(string command)
        {
            var dataList = new List<string>();
            var list = new List<Results>(SelectCommand(command, conn()));

            for (var i = 0; i < list.Count; i++)
            {
                dataList.Add($"{list[i].Row}");
            }

            return dataList;
        }

        public static string ShowInString(string command)
        {
            var dataList = new List<string>();
            var list = new List<Results>(SelectCommand(command, conn()));

            for (var i = 0; i < list.Count; i++)
            {
                dataList.Add($"{list[i].Row}");
            }

            return string.Join(",", dataList);
        }

        ////**/ DB Class /**////

        public class AddPerson
        {
            public string fname { get; set; }
            public string lname { get; set; }
            public DateTime dob { get; set; }
            public int streetno { get; set; }
            public string streetname { get; set; }
            public string postcode { get; set; }
            public string city { get; set; }
            public string phone1 { get; set; }
            public string phone2 { get; set; }
            public string email { get; set; }
            public int id { get; set; }

            public AddPerson(string _fname, string _lname, DateTime _dob, int _streetno, string _streetname, string _postcode, string _city, string _phone1, string _phone2, string _email)
            {
                fname = _fname;
                lname = _lname;
                dob = _dob;
                streetno = _streetno;
                streetname = _streetname;
                postcode = _postcode;
                city = _city;
                phone1 = _phone1;
                phone2 = _phone2;
                email = _email;
            }

            public AddPerson(int _id)
            {
                id = _id;
            }

            public AddPerson(string _fname, string _lname, DateTime _dob, int _streetno, string _streetname, string _postcode, string _city, string _phone1, string _phone2, string _email, int _id)
            {
                fname = _fname;
                lname = _lname;
                dob = _dob;
                streetno = _streetno;
                streetname = _streetname;
                postcode = _postcode;
                city = _city;
                phone1 = _phone1;
                phone2 = _phone2;
                email = _email;
                id = _id;
            }
        }

        ////**/ INSERT STATMENT /**////

        public static void AddData(string fname, string lname, string dob, string streetno, string streetname, string postcode, string city, string phone1, string phone2, string email)
        {
            var date = DateTime.Parse(dob);
            var streetNum = int.Parse(streetno);

            var customdb = new AddPerson(fname, lname, date, streetNum, streetname, postcode, city, phone1, phone2, email);

            var con = conn();
            con.Open();

            MySqlCommand insertCommand = con.CreateCommand();

            insertCommand.CommandText = "INSERT INTO tbl_people(FNAME, LNAME, DOB, Str_NUMBER, Str_NAME, POSTCODE, CITY, PHONE1, PHONE2, EMAIL)VALUES(@fname, @lname, @dob, @streetno, @streetname, @postcode, @city, @phone1, @phone2, @email)";
            insertCommand.Parameters.AddWithValue("@fname", customdb.fname);
            insertCommand.Parameters.AddWithValue("@lname", customdb.lname);
            insertCommand.Parameters.AddWithValue("@dob", customdb.dob);
            insertCommand.Parameters.AddWithValue("@streetno", customdb.streetno);
            insertCommand.Parameters.AddWithValue("@streetname", customdb.streetname);
            insertCommand.Parameters.AddWithValue("@postcode", customdb.postcode);
            insertCommand.Parameters.AddWithValue("@city", customdb.city);
            insertCommand.Parameters.AddWithValue("@phone1", customdb.phone1);
            insertCommand.Parameters.AddWithValue("@phone2", customdb.phone2);
            insertCommand.Parameters.AddWithValue("@email", customdb.email);
            insertCommand.ExecuteNonQuery();

            con.Clone();
        }

        ////**/ UPDATE STATMENT /**////

        public static void UpdateData(string fname, string lname, string dob, string streetno, string streetname, string postcode, string city, string phone1, string phone2, string email, string id)
        {
            var uid = int.Parse(id);
            var date = DateTime.Parse(dob);
            var streetNum = int.Parse(streetno);

            var customdb = new AddPerson(fname, lname, date, streetNum, streetname, postcode, city, phone1, phone2, email , uid);

            var con = conn();
            con.Open();

            MySqlCommand updateCommand = con.CreateCommand();


            updateCommand.CommandText = "UPDATE tbl_people SET FNAME = @fname, LNAME = @lname, DOB = @dob, Str_NUMBER = @streetno, Str_NAME = @streetname, POSTCODE = @postcode, CITY = @city, PHONE1 = @phone1, PHONE2 = @phone2, EMAIL = @email WHERE ID = @id";
            updateCommand.Parameters.AddWithValue("@fname", customdb.fname);
            updateCommand.Parameters.AddWithValue("@lname", customdb.lname);
            updateCommand.Parameters.AddWithValue("@dob", customdb.dob);
            updateCommand.Parameters.AddWithValue("@streetno", customdb.streetno);
            updateCommand.Parameters.AddWithValue("@streetname", customdb.streetname);
            updateCommand.Parameters.AddWithValue("@postcode", customdb.postcode);
            updateCommand.Parameters.AddWithValue("@city", customdb.city);
            updateCommand.Parameters.AddWithValue("@phone1", customdb.phone1);
            updateCommand.Parameters.AddWithValue("@phone2", customdb.phone2);
            updateCommand.Parameters.AddWithValue("@email", customdb.email);
            updateCommand.Parameters.AddWithValue("@id", customdb.id);
            updateCommand.ExecuteNonQuery();

            con.Clone();
        }

        ////**/ DELETE STATMENT /**////

        public static void DeleteDate(string id)
        {
            var uid = int.Parse(id);

            var customdb = new AddPerson(uid);

            var con = conn();
            con.Open();

            MySqlCommand updateCommand = con.CreateCommand();

            updateCommand.CommandText = "DELETE FROM tbl_people WHERE ID = @id";
            updateCommand.Parameters.AddWithValue("@id", customdb.id);
            updateCommand.ExecuteNonQuery();

            con.Clone();
        }

    }



}
